---
type : game
title : Dragon Quest Swords - The Masked Queen and the Tower of Mirrors (Europe) (En,Fr,De,Es,It)
format : rvz
archive : zip
link : Dragon%20Quest%20Swords%20-%20The%20Masked%20Queen%20and%20the%20Tower%20of%20Mirrors%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%29
size : 2.6
console : wii 
---
