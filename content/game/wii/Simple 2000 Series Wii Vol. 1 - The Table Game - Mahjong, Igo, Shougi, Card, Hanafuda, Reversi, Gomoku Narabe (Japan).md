---
type : game
title : Simple 2000 Series Wii Vol. 1 - The Table Game - Mahjong, Igo, Shougi, Card, Hanafuda, Reversi, Gomoku Narabe (Japan)
format : rvz
archive : zip
link : Simple%202000%20Series%20Wii%20Vol.%201%20-%20The%20Table%20Game%20-%20Mahjong%2C%20Igo%2C%20Shougi%2C%20Card%2C%20Hanafuda%2C%20Reversi%2C%20Gomoku%20Narabe%20%28Japan%29
size : 250.5
console : wii 
---
