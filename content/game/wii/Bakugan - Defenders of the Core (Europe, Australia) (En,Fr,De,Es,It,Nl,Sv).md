---
type : game
title : Bakugan - Defenders of the Core (Europe, Australia) (En,Fr,De,Es,It,Nl,Sv)
format : rvz
archive : zip
link : Bakugan%20-%20Defenders%20of%20the%20Core%20%28Europe%2C%20Australia%29%20%28En%2CFr%2CDe%2CEs%2CIt%2CNl%2CSv%29
size : 3.3
console : wii 
---
