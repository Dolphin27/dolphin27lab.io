---
type : game
title : Legend of Zelda, The - Skyward Sword (Europe) (En,Fr,De,Es,It) (Rev 1)
format : rvz
archive : zip
link : Legend%20of%20Zelda%2C%20The%20-%20Skyward%20Sword%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%29%20%28Rev%201%29
size : 3.9
console : wii 
---
