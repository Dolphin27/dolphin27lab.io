---
type : game
title : Legend of the Guardians - The Owls of Ga'Hoole (Europe) (En,Fr,De,Es,It,Nl)
format : rvz
archive : zip
link : Legend%20of%20the%20Guardians%20-%20The%20Owls%20of%20Ga%27Hoole%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%2CNl%29
size : 3.1
console : wii 
---
