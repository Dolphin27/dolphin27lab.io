---
type : game
title : Broken Sword - Shadow of the Templars - The Director's Cut (Europe) (En,Fr,De,Es,It)
format : rvz
archive : zip
link : Broken%20Sword%20-%20Shadow%20of%20the%20Templars%20-%20The%20Director%27s%20Cut%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%29
size : 3.4
console : wii 
---
