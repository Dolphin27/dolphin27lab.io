---
type : game
title : LEGO Pirates of the Caribbean - The Video Game (Europe) (En,Fr,De,Es,It,Nl,Sv,No,Da)
format : rvz
archive : zip
link : LEGO%20Pirates%20of%20the%20Caribbean%20-%20The%20Video%20Game%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%2CNl%2CSv%2CNo%2CDa%29
size : 1.4
console : wii 
---
