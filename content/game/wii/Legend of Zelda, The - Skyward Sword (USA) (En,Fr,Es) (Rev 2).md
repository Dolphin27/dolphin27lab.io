---
type : game
title : Legend of Zelda, The - Skyward Sword (USA) (En,Fr,Es) (Rev 2)
format : rvz
archive : zip
link : Legend%20of%20Zelda%2C%20The%20-%20Skyward%20Sword%20%28USA%29%20%28En%2CFr%2CEs%29%20%28Rev%202%29
size : 3.7
console : wii 
---
