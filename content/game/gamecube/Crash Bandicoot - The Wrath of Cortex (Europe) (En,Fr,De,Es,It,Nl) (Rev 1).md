---
type : game
title : Crash Bandicoot - The Wrath of Cortex (Europe) (En,Fr,De,Es,It,Nl) (Rev 1)
format : rvz
archive : zip
link : Crash%20Bandicoot%20-%20The%20Wrath%20of%20Cortex%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%2CNl%29%20%28Rev%201%29
size : 493.2
console : gamecube 
---
