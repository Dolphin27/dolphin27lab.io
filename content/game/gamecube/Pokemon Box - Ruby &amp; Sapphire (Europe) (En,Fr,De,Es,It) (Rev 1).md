---
type : game
title : Pokemon Box - Ruby &amp; Sapphire (Europe) (En,Fr,De,Es,It) (Rev 1)
format : rvz
archive : zip
link : Pokemon%20Box%20-%20Ruby%20%26%20Sapphire%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%29%20%28Rev%201%29
size : 63.2
console : gamecube 
---
