---
type : game
title : Game Boy Player Start-Up Disc (Europe) (En,Fr,De,Es,It) (Rev 1)
format : rvz
archive : zip
link : Game%20Boy%20Player%20Start-Up%20Disc%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%29%20%28Rev%201%29
size : 6.8
console : gamecube 
---
