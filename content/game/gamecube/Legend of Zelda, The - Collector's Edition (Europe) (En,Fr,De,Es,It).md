---
type : game
title : Legend of Zelda, The - Collector's Edition (Europe) (En,Fr,De,Es,It)
format : rvz
archive : zip
link : Legend%20of%20Zelda%2C%20The%20-%20Collector%27s%20Edition%20%28Europe%29%20%28En%2CFr%2CDe%2CEs%2CIt%29
size : 872.3
console : gamecube 
---
