base=/sdcard/alarms

rm -rf $base/m
mkdir -p $base/m

input=$base/a.txt
while IFS= read -r line
do

LINK=`echo "$line" | sed -E 's/^link=//; s/\.zip.*$//'`
NAME=`echo "$line" | sed -E 's/.*title=//; s/\.zip.*$//'`
SIZE=`echo "$line" | sed -E 's/.*size=//; s/ .*//g'`
F=$base/m/${NAME}.md
echo "$NAME"
echo "---" > "$F"
echo "type : game" >> "$F"
echo "title : $NAME" >> "$F"
echo "format : rvz" >> "$F"
echo "archive : zip" >> "$F"
echo "link : $LINK" >> "$F"
echo "size : $SIZE" >> "$F"
echo "console : wii " >> "$F"
echo "---" >> "$F"

done < "$input"

